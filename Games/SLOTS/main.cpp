/* 
 * File:   main.cpp
 * Author: Owner
 * Name: Ryan Fingerle
 * ID:2419802
 * Work: SLOTS for Game/Project
 * 
 * Problem: 
 *
 * Created on October 19, 2014
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <ctime>



using namespace std;

void clearscreen()
{
    for (int i =0; i<50; i++)
    {
        cout << endl;
    }
}
int Random(int low, int high) 
{
int rand_num = low + rand() % ((high + 1) - low );
return rand_num;
}
/*
 * 
 */
int main(int argc, char** argv) {

int low1(2), low2(2), low3(2); 
int high1(7), high2(7), high3(7);
int cash = 1000;
int num;
int bet;
string name; 

cout << "Welcome to Ryan's Simple Slot Machine Game!\n"; 
cout << "Please Enter your Name: \n"; 
cin >> name; 
cout << name << "´s chips: $" << cash << endl;

while (true) 
{

cout << "1) Play slot. 2) Exit. ";
cin >> num;


if (num == 2)
{
cout << "Exiting..." << endl;
break;
}


else if (num == 1) 
{
cout << "Enter your bet: ";
cin >> bet;

if (bet > cash || bet < 0 || bet == cash || bet == 0) 
{
cout << "You did not enter a valid bet." << endl;
}

else
{ 
srand( time(0) );

int first = Random(low1, high1);
int second = Random(low2, high2);
int third = Random(low3, high3);
int fourth = Random(low1, high3);
int fifth = Random(low2, high2); 
int sixth = Random(low3, high1); 
int seventh = Random(low1, high3);
int eighth = Random(low2, high1); 
int ninth = Random(low3, high2);

string s1 = "You won!"; 

clearscreen();
cout << name << "'s Slot Machine Game\n";
cout << "---------------------------------------------\n";
cout << first << " " << second << " " << third << endl;
cout << fourth << " " << fifth << " " << sixth << endl;
cout << seventh << " " << eighth << " " << ninth << endl;
cout << "-----------------------------------------------\n";

if (first == 7 && second == 7 && third == 7)
{
cout << s1 << endl;
cash += bet * 10;
}
else if (first == second == third)
{
cout << s1 << endl;
cash += bet * 5;
}
else if (first == second || second == third || first == third)
{
cout << s1 << endl;
cash += bet * 3;
}
else
{
cout << "You lose!" << endl;
cash -= bet;
}

if (cash < 0)
{
cout << "You have no more money left!" << endl;
cout << "Exiting..." << endl;
break;
}

cout << name << "´s chips: $" << cash << endl;


}

}

else
{
cout << "You did not enter a valid number!" << endl;
}

}


    return 0;
}

