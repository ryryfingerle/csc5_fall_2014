/* 
 * File:   main.cpp
 * Author: Owner
 * Name: Ryan Fingerle
 * ID: 2419802
 *
 * Created on October 28, 2014,
 */

#include <cstdlib>
#include <iostream>
#include <string>
#include <fstream>
#include <vector>

using namespace std;

// Number 2 on LBWK10 
//Write the function that stores values 50-100 in vector size 50
void store(vector<int>&v)
{
    for( int i=0; i < v.size(); i++)
    {
        v[i] = i;
    }
}

//For Number 3
void showValues(vector<int>);

/*
 * 
 */
int main(int argc, char** argv) {
   
    
    
    // Number 1 on LBWK10 
    // reads the file from the user and correct the errors and stores values into vector
    string input; // contents in the file
     fstream userFile; // file stream object 
    
     //open the file in input mode
      userFile.open("error.check.txt", ios::in);
    
     // if the file opens successfully, continue
        if (userFile)
        {
            //read the line in the file 
            getline(userFile, input);
            
            //read successfully
            while(userFile)
            {
                //Display the last item read
                cout << input << endl; 
                
                //read the next item
                getline(userFile, input);
            }
            // close the file 
            userFile.close();
        }
        else 
        {
            cout << "ERROR: Cannot Read the File\n";
        }
      
std::ifstream ifs( "error.check.txt" );

/*std::vector< double > values; // to look for the values 
double val;
while( ifs >> val )
   values.push_back( val );  // increment the value
*/

//Number 2 on LBWK10 
// See the function header above for the function created 

//Number 3 on LBWK10 
// check whether if the user input value is inside the vector

vector<int>values; 
// Put the numbers in the vector
for(int count = 0;  count < 7; count++)
{
    values.push_back(count * 2);
}
//display the numbers 
showValues(values);


    return 0;
}

//For Number 3
void showValues(vector<int>vect)
{
    for(int count = 0; count <vect.size(); count++)
        cout << vect[count] << endl;
}