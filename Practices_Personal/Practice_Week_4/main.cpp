/* 
 * File:   main.cpp
 * Author: Owner
 *
 * Created on September 24, 2014, 2:51 PM
 */

#include <cstdlib>
#include <iostream>    // std::cout, std::fixed
#include <iomanip>      // std::setprecision
#include <fstream>      // std::inputfile

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
 
// Using the codes ++ & --    
int num = 4;     //num starts with a 4 

// Display the value in num
cout << " The variable num is " << num << endl;
cout << "I will now increment num. \n\n"; 

// Use postfix  ++ to increment num 
num++; 
cout << "Now the variable num is " << num << endl; 
cout << "I will increment num again. \n\n"; 

// Use prefix  ++  to increment num 
++num; 
cout << "Now the variable num is " << num << endl; 
cout << "I now will decrement num. \n\n"; 

// Use postfix -- to decrement num.
num --; 
cout << "Now the variable num is " << num << endl; 
cout << "I will decrement the num again. \n\n";

// Use prefix -- to decrement num 
--num; 
cout << "Now the variable num is " << num << endl;


// While-LOOP 
// This program demonstrates a simple while loop 

int number = 1; 

while (number <=5)
{
cout << "Hello\n";
number++; 
}
cout << "That's all!\n"; 

// Testing the user input number using the while loop

cout << "Enter a number less than 10: ";
cin >> number; 
while (number <= 10)
{
cout << "Invalid Entry!\n"
<< "Enter a number less than 10: ";
cin >> number;
}

//Input Validation

//Get the number of players available 
cout << "How many players are available? "; 
int players;
cin >> players; 

//Validate the input 
while(players <= 0)
{
cout << "Please enter a positive number: ";
cin >> players;
}

//This program displays the numbers 1 through 10 and their squares 
int numx =1;  // Initialize the counter

cout << "Number       Number Squared\n"; 
cout << "-------------------------------------\n"; 
while (numx <=10)
{
cout << numx << "\t\t" << (numx*numx) << endl;
numx++; // Increment the counter 
}

//This program average 3 test scores. It repeats as many times as the user wishes
int score1, score2,  score3; // Three scores 
double average;                      // Average score 
char again;                               // to hold Y or N input 

do 
{ 
// Get three scores.. 
cout << " Enter 3 scores and I will average them: "; 
cin >> score1 >> score2 >> score3; 

//Calculate and display the average
average = (score1 + score2 + score3)  / 3.0;
cout << " The average is = " << average << ".\n";

// Does the user want to average another set? 
cout << " Do you want to average another set of scores? (Y/N) "; 
cin >> again; 
} while (again == 'Y' || again == 'y');

//Trying this program called test 

int x, y; 
for (x=1, y=1; x <= 5;  x++, y++)
{
cout << x << " plus " << y  << " equals " << (x+y) << endl;
}

//This program takes daily sales figures over a period of time and calculates their total 
int days;    // number of days 
double total = 0.0; // accumulator, initialized with 0 

// get the number of days 
cout << " For how many days do you have sales figures?" ; 
cin >> days; 

// Get the sales for each day and accumulate a total 
for (int count = 1; count <= days; count++)
{ 
double sales; 
cout << "Enter the sales for day " << count << ": " ; 
cin >> sales;
total += sales; // accumulate the running total 
}

//Display the total sales 
cout << fixed << showpoint << setprecision (2); 
cout << "The total sales are $ " << total << endl;

// This program calculates the total number of points a soccer team has earned 
// over a series of games. The user enters a series of point values, then -1 when finished

int game = 1,                   // game counter 
      points,                        // to hold a number of points 
      total2= 0;                   // accumulator

cout << "Enter the number of points your team has earned\n"; 
cout << "So far in the season, then enter -1 when finished. \n\n";
cout << "Enter the points for game " << game << ": "; 
cin >> points; 

while (points != -1) 
{
total2 += points; 
game++; 
cout << "Enter the points for game " << game << ": "; 
cin >> points; 
}
cout << "\nThe total points are " << total2 << endl;


//This program displays all of the numbers in a file

ifstream inputFile;      //File stream object
int number2;               // to hold a value from the file

inputFile.open("numbers.txt");   //open the file 
if (! inputFile)                                    //test for errors
cout << "Error opening file.\n";
else 
{
while (inputFile >> number) // read a number 
{ 
cout << number << endl;  //display the number 
}
inputFile.close();  //close the file
}
    return 0;
}

