/* 
 * File:   main.cpp
 * Author: Owner
 *
 * Created on September 23, 2014, 12:00 AM
 */

#include <cstdlib>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

string name; 
string name2; 
string food; 
string number; 
string adjective; 
string color; 
string animal; 

cout << " Please type your name " << endl;  
cin >> name; 
    
cout << " Please type your least favorite teacher's name" << endl; 
cin >> name2; 

cout << "Please type your favorite food" << endl;
cin >> food; 

cout << "Please type your number between 100-120" << endl; 
cin >> number; 

cout << "Please type the best adective you can describe for a dog" << endl; 
cin >> adjective; 

cout << "Please type the color that you think people will turn into when they feel sick" << endl; 
cin >> color; 

cout << " Please type in your favorite pet" << endl;
cin >> animal; 

cout << "Now-- Watch how your words get inserted in this paragraph" << endl;

cout << "Dear " << name2 << endl; 
cout << "I am sorry that I am unable to turn in my homework at this time. First, I ate a rotten " << food
           << ", which made me turn " << color << " and extremenly ill. I came down with a fever of " << number 
           << " . Next, my " << adjective << " pet " << animal << " must have smelled the remains of the " 
           <<  food << " on my homework because  he ate it. I am currently rewritting my homework and hope you "
          << " will accept it late. " << endl; 

cout << "Sincerely, " << endl;
cout << name << endl; 
    
    return 0;
}

