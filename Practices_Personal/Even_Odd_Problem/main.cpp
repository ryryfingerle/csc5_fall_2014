/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 11, 2014, 11:30 AM
 */

#include <cstdlib>
#include <iostream>


using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    // Determine if a number is even or odd
    
    // Prompt the user 
    
    cout << "Please enter a number: " << endl; 
    
    // Declare variable 
    
    int userInput; 
    
    cin >> userInput;
    
    // Conditional if statement 
    if (userInput % 2 == 0) 
    { 
      cout << "Even Number!" << endl;
    }     

    else
    { 
        cout << "Odd Number!" << endl;
    }    
    
    return 0;
}

