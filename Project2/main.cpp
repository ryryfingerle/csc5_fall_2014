/* 
 * File:   main.cpp
 * Author: Owner
 * Name: Ryan Fingerle 
 * ID: 2419802
 * 
 * TICTACTOE
 * MORTGAGE CALCULATOR 
 *
 * Created on December 3, 2014
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cmath>
#include <limits>

using namespace std;
//TIC TAC TOE FUNCTION 
char square[10] = {'0','1','2','3','4','5','6','7','8','9'}; //array for the board
void clearscreen()  //Clear the screen each menu and game 
{
    for (int i =0; i<50; i++)
    {
        cout << endl;
    }
}
int checkwinorlose();//see function at the bottom
void board(); //see function at the bottom


/*
 * 
 */
int main(int argc, char** argv) {
      // Constants for Menu Choices
    const int TICTACTOE_OPTION = 1, 
                       MORTGAGE_OPTION = 2;
            
 // Variables
    int OPTION;   // Menu Choice
    
    // Set up Numeric Output Formatting 
    cout << fixed << showpoint << setprecision(2);
    
    //Display the Menu 
    do
    {
        clearscreen();
    cout << "\n\t\tLet's Play a Game or Calculate Your Estimate Mortgage Cost! \n\n"
            << "1. TIC TAC TOE  \n" 
            << "2. MORTGAGE CALCULATOR \n"; 
            
          
cout  << "Enter your choice: "; 
    cin >> OPTION; //user prompt choice
    
    
    // Process the User's choice
    if (OPTION == TICTACTOE_OPTION)
    {
   clearscreen(); 
  //Variables for tic tac toe
 int player = 1;
 int i;
 int choice;
 
char mark;
do
{
board();
player=(player%2)?1:2;



cout << "Player " << player << ", enter a number:  ";
cin >> choice;
clearscreen();
mark=(player == 1) ? 'X' : 'O';

if (choice == 1 && square[1] == '1')

square[1] = mark;
else if (choice == 2 && square[2] == '2')

square[2] = mark;
else if (choice == 3 && square[3] == '3')

square[3] = mark;
else if (choice == 4 && square[4] == '4')

square[4] = mark;
else if (choice == 5 && square[5] == '5')

square[5] = mark;
else if (choice == 6 && square[6] == '6')

square[6] = mark;
else if (choice == 7 && square[7] == '7')

square[7] = mark;
else if (choice == 8 && square[8] == '8')

square[8] = mark;
else if (choice == 9 && square[9] == '9')

square[9] = mark;
else
{
cout<<"Invalid move ";

player--;

}
i=checkwinorlose();

player++;
}while(i==-1);
board();
if(i==1)

cout<<"==>\a     Player "<<--player<<" wins!!   <== ";
else
cout<<"==>\a     Game Draw    <==";
    }
     if (OPTION == MORTGAGE_OPTION)
    {
        clearscreen();
         cout << " ==========>>> Ryan's Loan Calculator <<<============="<<endl;
    cout << " This app will help you figure whether or not if you could afford " << endl;
    cout  <<" a particular loan and see how much each month that you have to pay.\n" << endl;
    
    //Prompt the user for information 
    double homevalue;  // total amount of asking price for calculations
    double interestrate; 
    double loanamount; //total amount of loan for calculation 
    double downpayment; //user prompt for down payment 
    
    double annualincome; 
    double creditcardspayment;
    double carloans; 
    double carpayment;
    double studentloans; 
    double studentpayment; 
    double personalloans; 
    double personaloanpayment; 
    double t; // terms for calculation
double periods;
double payment;
double mRate;
double conversion;
const double months = 12;
int paymentNumber;
double bal;
int arraySelectTerm;
int arraySelectRate;
    // define 
double principle;
double term[] = {7, 15, 30};
double rate[] = {4.75, 5.5, 7.25};
double r;
double te;
int choice1;
int insurance;
int MonthlyIncome; // annual income calculated
int DebtRatio; 
float Number_of_Years; // for 7-20-30 years - user prompt
float MonthlyInterest; 
int number_Of_Payments; // for calculations  
float payment2; //Estimated amount to pay each month



    cout << "House Value  [The Asking/Listing Price]:  " << endl;
    cin >> homevalue; 
    cout << "Annual Income:    " << endl; 
    cin >> annualincome; 
    cout << "Interest Rate (%): " << endl;
    cin >> interestrate; 
    cout << "Loan Amount:  " << endl;
    cin >> loanamount;
    cout << "How many years [7-20-30]: " << endl;
    cin >> Number_of_Years;
    
    cout << "Down Payment (percent):  " << endl;
    cin >> downpayment; 
    
  //if you see the statements in grey area which means they are extra, possibly not needed. 
    cout << " Monthly Debt To Your Credit Cards: " << endl;
    cin >> creditcardspayment; 
    //cout << "Car Loans Amount Owed (Total Amount): " << endl;
    //cin >> carloans; 
    cout << "Monthly Payment To Your Car Loans: " << endl;
    cin >> carpayment; 
   // cout << "Student Loans Amount Owed (Total Amount): " << endl;
   // cin >> studentloans; 
    cout << "Monthly Payment To Your Student Loans: " << endl;
    cin >> studentpayment; 
    //cout << "Personal Loans Amount Owed (Total Amount): " << endl;
    //cin >> personalloans; 
    cout << "Monthly Payment To Your Personal Loans: " << endl;
    cin >> personaloanpayment; 
    
 //Calculate Grossed  Monthly Income
MonthlyIncome = (annualincome)/12;
double EstimatedDT;
double TotalDebt;
cout << "Your Monthly Income is $ " << MonthlyIncome << endl;
cout << endl;
TotalDebt = (creditcardspayment + carpayment + studentpayment + personaloanpayment);

cout << "Your Total Debt is " << TotalDebt << endl;

EstimatedDT = TotalDebt / MonthlyIncome;

cout << "Your Debt to Income is " << EstimatedDT << endl;
cout << endl;

//Calculate the total  debt to income 
//Still didn't calculate correctly 
DebtRatio = ((carpayment + studentpayment + personaloanpayment)/(MonthlyIncome));
if ( DebtRatio <= .43 ) 
{  
    cout << "Your Debt to Income is $ " << DebtRatio << endl; //decimal? 
    cout << "You qualify for the mortgage loan!" << endl;
}
else if   ( DebtRatio > .43)
{   
    cout << "Your Debt to Income is $ " << DebtRatio << endl; 
        cout << "I am sorry. You do not qualify for the mortgage loan" << endl;
}
        
        //Calculate the values
MonthlyInterest = (interestrate/100)/12; 

number_Of_Payments = Number_of_Years * 12; 



//Output 
cout << "For Loan Amount: $" << homevalue << " with interest rate: " << interestrate << " and a "
        << Number_of_Years << " year mortgage" << "." << endl;
cout << endl;


  


 cout << "Examples For You To Calculate the Rates in Excel" << endl;
 cout << endl;
cout << "Enter the amount of the mortgage in US dollars " << endl;
cout << endl;
cin >> principle;
cout << endl;

cout << "Select the loan terms " << endl;
cout << "0 - " << term[0] << " years and " << rate[0] << "%" << endl;
cout << "1 - " << term[1] << " years and " << rate[1] << "%" << endl;
cout << "2 - " << term[2] << " years and " << rate[2] << "%" << endl;
cin >> arraySelectTerm;
if (arraySelectTerm == 0) {
te = term[0];
r = rate[0];
}
if (arraySelectTerm == 1) {
te = term[1];
r = rate[1];
}
if (arraySelectTerm == 2) {
te = term[2];
r = rate[2];
}



// Output to be displayed
cout << "Mortgage amount: $" << principle << endl; //output amount
cout << "Mortgage term in years: " << te << endl; //output length of loan
cout << "Mortgage interest rate: " << r << endl; //output interest rate
cout << endl;
cout << endl;

// Convert to monthly interest rate
mRate = (r * .01) / months;

// Convert years to months
periods = te * months;

// Create variable for (1 + rate)periods
t = pow(1 + mRate, periods);

// Calculate payment
payment= (principle * t * mRate)/(t - 1);


    

// Output Monthly payment
cout << "Monthly payment is $" << payment << endl;

// Output Amortization
cout << "Loan Balance      Interest Paid     Principle Payment" << endl;
cout << "-----------------------------------------------------" << endl;
bal = principle; //initialize outside the loop
int count = 0;
for (paymentNumber = 1; paymentNumber <= periods; ++paymentNumber , count++)
{

 cout << bal << "          ";
 cout << bal *mRate << "          "; //interest payment
 cout << payment - bal*mRate << endl; //principle payment
 bal -= payment - bal*mRate; //subtract principle payment from bal
 if( count == 10)
 {
   cin.get();
   cout<<endl;
   cout << "Loan Balance      Interest Paid     Principle Payment" << endl;
   cout << "-----------------------------------------------------" << endl;

   count = 0;
 }


}

        
     }    
while (OPTION < TICTACTOE_OPTION || OPTION > MORTGAGE_OPTION);
        
    
        cout << "Please enter a valid menu choice: "; 
        cin >> OPTION;
        
 }while (OPTION != TICTACTOE_OPTION);

    return 0;
}

/*********************************************
 * Functions TIC TAC TOE
**********************************************/

int checkwinorlose()
{
	if (square[1] == square[2] && square[2] == square[3])

		return 1;
	else if (square[4] == square[5] && square[5] == square[6])

		return 1;
	else if (square[7] == square[8] && square[8] == square[9])

		return 1;
	else if (square[1] == square[4] && square[4] == square[7])

		return 1;
	else if (square[2] == square[5] && square[5] == square[8])

		return 1;
	else if (square[3] == square[6] && square[6] == square[9])

		return 1;
	else if (square[1] == square[5] && square[5] == square[9])

		return 1;
	else if (square[3] == square[5] && square[5] == square[7])

		return 1;
	else if (square[1] != '1' && square[2] != '2' && square[3] != '3' 
                    && square[4] != '4' && square[5] != '5' && square[6] != '6' 
                  && square[7] != '7' && square[8] != '8' && square[9] != '9')

		return 0;
	else
		return -1;
}


/******************************************
     FUNCTION TO DRAW BOARD OF TIC TAC TOE 
*******************************************/


void board()
{

cout << "\n\n\tRyan's Tic Tac Toe\n\n";

cout << "Player 1 (X)  -  Player 2 (O)" << endl << endl;
cout << endl;

cout << "     |     |     " << endl;
cout << "  " << square[1] << "  |  " << square[2] << "  |  " << square[3] << endl;

cout << "_____|_____|_____" << endl;
cout << "     |     |     " << endl;

cout << "  " << square[4] << "  |  " << square[5] << "  |  " << square[6] << endl;

cout << "_____|_____|_____" << endl;
cout << "     |     |     " << endl;

cout << "  " << square[7] << "  |  " << square[8] << "  |  " << square[9] << endl;

cout << "     |     |     " << endl << endl;

    
}