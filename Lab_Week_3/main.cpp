/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 11, 2014, 11:44 AM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>




using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    // Number 1 on Lab Week 3 
    
    string var1 = "1";
    
    int var2 = 2;
    
    cout << var1 << " + " << var1 << " = " << var1 + var1 << endl; 
    cout << var2 << " + " << var2 << " = " << var2 + var2 << endl;
    
    // Number 2 on Lab Week 3
    
    cout << "To Determine your Slugging Percentage" << endl;
    
    int singles;
    int doubles;
    int triples;
    int home_runs;
    int at_bats;
    
    cout << "Input your Singles" << endl;
    
    cin >> singles;
    
    cout << "Input your Doubles" << endl; 
    
    cin >> doubles; 
    
    cout << "Input your Triples" << endl; 
    
    cin >> triples; 
    
    cout << "Input your Home Runs" << endl;
    
    cin >> home_runs; 
    
    cout << "Input your At Bats" << endl;
    
    cin >> at_bats;
    
    double avg = (singles + 2) * (doubles + 3) * (triples + 4) * 
                 (home_runs) / static_cast<double>(at_bats);
    
    cout << "Your Sluggish Percentage is " << avg << endl;
    
    // Number Three on Lab Week 3
    
    int number1;
    int number2; 
    
    cout << "Input a number for a x" << endl; 
    
    cin >> number1;
    
    cout << "Input a number for a y" << endl; 
    
    cin >> number2;
    
    cout << "Now watch how your X and Y converts your numbers" << endl; 
    
    cout << "x = " << number1 << "" << " y = " << number2 << endl; 
    cout << "x = " << number2 << "" << " y = " << number1 << endl;
    
    
    //Number 4 on Lab Week 3
    
    int a = 4;
    int z = 0; 
    
    if (a == 4)
    {
        z = 4;
    }
    else if (a == 9)
    {
        z = 5;
    }
    else 
    {
        z = 6;
    }
    
    
// Interesting no output for this code
    // Is it because of no cout? 
    
    // Number 5 on Lab Week 3 
    
   //Variables
	double amountPaid = 0.0;
	double amountOwed = 0.0;
	double difference = 0.0;
	const double dollar = 1.0;
	const double quarter = 0.25;
	const double dime = 0.1;
	const double nickel = 0.05;
	const double penny = 0.01;
	int dollarsNeeded = 0;
	int quartersNeeded = 0;
	int dimesNeeded = 0;
	int nickelsNeeded = 0;
	int penniesNeeded = 0;
	double currentOwed = 0.0;

//Input
	cout << "Please enter the amount owed: ";
	cin >> amountOwed;
	cout << "Please enter the amount paid: ";
	cin >> amountPaid;

//Calculations
	difference = amountPaid - amountOwed;
	dollarsNeeded = difference/dollar;
	quartersNeeded = (difference - dollarsNeeded) / quarter;
	dimesNeeded = (difference - dollarsNeeded - quartersNeeded*quarter) / .1;
	nickelsNeeded = (difference - dollarsNeeded - quartersNeeded*quarter - dimesNeeded*dime) / .5;
	penniesNeeded = (difference - dollarsNeeded - quartersNeeded*quarter - dimesNeeded*dime - nickelsNeeded*nickel) / .01;

//Output
	if (amountPaid >= amountOwed)    //Subtracts the amount owed from the payment, then displays exact change
{		cout << "The change is: " << difference << endl;
		cout << "Please give the customer: " << endl;
		cout << dollarsNeeded << " Dollars" << endl;
		cout << quartersNeeded << " Quarters" << endl;
		cout << dimesNeeded << " Dimes" << endl;
		cout << nickelsNeeded << " Nickels" << endl;
		cout << penniesNeeded << " Pennies" << endl;

}
	else
{
		currentOwed = (amountOwed - amountPaid);
		cout << "The customer still owes: " << currentOwed << endl;
        }
//Number 6 on Lab Week 3 

  double x = 10.76;
  double y = 1.50;
  const int mult = 100;
  
  double dif = x - y;
  cout << dif << endl;
  
  int intDif = static_cast<int>(dif * mult);

  cout << intDif << endl;
  cout << dif * mult << endl;
// output >>
 // 9.26
  // 926
  //926
    
    return 0;
}

