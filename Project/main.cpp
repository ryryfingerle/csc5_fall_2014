/* 
 * File:   main.cpp
 * Author: Owner
 * Name: Ryan Fingerle
 * Project1:  Slots/Blackjack/5 cardpoker
 *
 * 
 * Problem: BLACKJACK
 * Ace is not identified as 11 or 1 so it cause confusion in this game 
 * ---- Example if Queen & Ace [11] = Blackjack! but Player: 13 which means Ace [1]  Output= You Win! 
 * ---- If Dealer and you happened to get 11 at the same time just input 's' in order for the Blackjack comment
 * 
 *  ------Repeatable  Loop "Would You like to play again?" starts before the "wager" 
 * 
 * Problem: SLOTS
 * Nothing so far I think of..
 * 
 * Created on October 19, 2014
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <ctime>
#include <cmath>


using namespace std;
void clearscreen()  //Clear the screen each menu and game 
{
    for (int i =0; i<50; i++)
    {
        cout << endl;
    }
}
int Random(int low, int high)  // for the SLOTS Random Numbers
{
int rand_num = low + rand() % ((high + 1) - low );
return rand_num;
}

/*
 * 
 */
int main(int argc, char** argv) {
    
    // Constants for Menu Choices
    const int SLOTS_CHOICE = 1, 
                       BLACKJACK_CHOICE = 2, 
                       CARDPOKER_CHOICE = 3;
                        
    
    // Variables
    int choice;   // Menu Choice
    
    // Set up Numeric Output Formatting 
    cout << fixed << showpoint << setprecision(2);
    
    //Display the Menu 
    
    do
    {
        clearscreen();
    cout << "\n\t\tLet's Play a Game! \n\n"
            << "1. SLOTS \n"
            << "2. BLACKJACKS\n"
            << "3. CARD POKER \n "
          
            << "Enter your choice: "; 
    cin >> choice;
    while (choice < SLOTS_CHOICE || choice > CARDPOKER_CHOICE);
        
    {
        cout << "Please enter a valid menu choice: "; 
        cin >> choice;
    }
    // Process the User's choice
    if (choice == SLOTS_CHOICE)
    {
        int low1(2), low2(2), low3(2); 
int high1(7), high2(7), high3(7);
int cash = 1000;
int num;
int bet;
string name; 

cout << "Welcome to Ryan's Simple Slot Machine Game!\n"; 
cout << "Please Enter your Name: \n"; 
cin >> name; 
cout << name << "´s chips: $" << cash << endl;

while (true) 
{

cout << "1) Play slot. 2) Exit. ";
cin >> num;


if (num == 2)
{
cout << "Exiting..." << endl;
break;
}


else if (num == 1) 
{
cout << "Enter your bet: ";
cin >> bet;

if (bet > cash || bet < 0 || bet == cash || bet == 0) 
{
cout << "You did not enter a valid bet." << endl;
}

else
{ 
srand( time(0) );

int first = Random(low1, high1);
int second = Random(low2, high2);
int third = Random(low3, high3);
int fourth = Random(low1, high3);
int fifth = Random(low2, high2); 
int sixth = Random(low3, high1); 
int seventh = Random(low1, high3);
int eighth = Random(low2, high1); 
int ninth = Random(low3, high2);

string s1 = "You won!"; 

clearscreen();
cout << name << "'s Slot Machine Game\n";
cout << "---------------------------------------------\n";
cout << first << " " << second << " " << third << endl;
cout << fourth << " " << fifth << " " << sixth << endl;
cout << seventh << " " << eighth << " " << ninth << endl;
cout << "-----------------------------------------------\n";

if (first == 7 && second == 7 && third == 7)
{
cout << s1 << endl;
cash += bet * 10;
}
else if (first == second == third)
{
cout << s1 << endl;
cash += bet * 5;
}
else if (first == second || second == third || first == third)
{
cout << s1 << endl;
cash += bet * 3;
}
else
{
cout << "You lose!" << endl;
cash -= bet;
}

if (cash < 0)
{
cout << "You have no more money left!" << endl;
cout << "Exiting..." << endl;
break;
}

cout << name << "´s chips: $" << cash << endl;


}

}

else
{
cout << "You did not enter a valid number!" << endl;
}

}

    }
    if (choice == BLACKJACK_CHOICE)
    {
        clearscreen();
        srand((unsigned)time(0));
	cout << "Welcome to Ryan's Unfair & Complicated Blackjack Game! \n";
	int wager;
                           int num;
	
                       
        while (true)
        {
            cout << "\n " << "1) Play Again 2) Exit" << endl;
                    cin >> num; 
                    if  (num == 2) 
                    {
                        cout << "Quitting the Game" << endl;
                        break;
                    }
                    else if (num == 1)
                    {
        
                           cout << "\n" << "Please place your wager: $";
	cin >> wager;	//wager
	int dealer_card1 = rand() % 13 + 1;  //dealer card 1 
	int dealer_card2 = rand() % 13 + 1;	 //dealer card 2
                    int dealer_card3 = rand() % 13 + 1;	 //dealer card 3
	int player_card1 = rand() % 13 + 1;	 //player card 1
	int player_card2 = rand() % 13 + 1;  //player card 2
                    int player_card3 = rand() % 13 + 1;  //player card 3 (For a chance)
     
        string hit_stand;
        
        
        
	cout << "\n" << "The dealer has "; 
	switch (dealer_card1) {
		case 1: cout << "Ace and ";
			break;
		case 11: cout << "Jack and ";
			break;
		case 12: cout << "Queen and ";
			break;
		case 13: cout << "King and ";
			break;
		default: cout << dealer_card1 << " and ";
			break;
}	
        if (dealer_card1 == 10|dealer_card1 ==11|dealer_card1 ==12|dealer_card1 ==13)
        {
           dealer_card1 = 10;
            
        }
        
        
	switch (dealer_card2) {
		case 1: cout << "Ace"; 
			break;
		case 11: cout << "Jack"; 
			break;
		case 12: cout << "Queen"; 
			break;
		case 13: cout << "King"; 
			break;
		default: cout << dealer_card2;
			break;
}	
          if (dealer_card2 == 10|dealer_card2 ==11|dealer_card2 ==12|dealer_card2 ==13)
        {
           dealer_card2 = 10;
            
        }
        
	cout << "\n" << "\n" << "You have ";
	switch (player_card1){
		case 1: cout << "Ace and "; 
		break;
		case 11: cout << "Jack and " ; 
			break;
		case 12: cout << "Queen and "; 
			break;
		case 13: cout << "King and "; 
			break;
		default: cout << player_card1 << " and "; 
			break;
}	
       if (player_card1 == 10|player_card1 ==11|player_card1 ==12|player_card1 ==13)
        {
            player_card1 = 10;
            
        }
 
            
        
	switch (player_card2){
		case 1: cout << "Ace"; 
			break;
		case 11: cout << "Jack" ; 
			break;
		case 12: cout << "Queen"; 
			break;
		case 13: cout << "King"; 
			break;
		default: cout << player_card2;
			break;
	
}

 if (player_card2 == 10|player_card2 ==11|player_card2 ==12|player_card2 ==13)
        {
            player_card2 = 10;
            
        }
        
	


        
        cout << "\n";
     
                
                
                
        
        
        
        
        
	int dealer_total = dealer_card1 + dealer_card2;
	int player_total = player_card1 + player_card2;
        
cout <<"\nDealer: " << dealer_total << " & " <<"Player: " << player_total ;  //Shows the numbers in case if confusion

if ((dealer_card1 == 1) && (dealer_card2 == 10))
		cout << "\nBlackjack! The Dealer Wins!\n"; 
          
else if ((dealer_card2 == 1) && (dealer_card1 == 10))
			cout << "\nBlackjack! The Dealer Wins!\n "; 
else if ((player_card1 == 1) && (player_card2 == 10))
		cout << "\nBlackjack! You Win $ " << wager*1.5 << "!!" << endl; 
          
	else if ((player_card2 == 1) && (player_card1 == 10))
			cout << "\nBlackjack! You Win $ " << wager*1.5 << "!!" << endl;
else 
{
           cout<<"\nHit or Stand: [S/H]";
                cin>>hit_stand;
                if (hit_stand == "h" | hit_stand == "H")
    {
       player_total = (player_total + player_card3);
      if (dealer_total <= 16)
      {
          dealer_total = (dealer_total  + dealer_card3); 
      
       cout << "\n" << "Now the dealer has "; 
	switch (dealer_card1) {
		case 1: cout << "Ace and ";
			break;
		case 11: cout << "Jack and ";
			break;
		case 12: cout << "Queen and ";
			break;
		case 13: cout << "King and ";
			break;
		default: cout << dealer_card1 << " and ";
			break;
}	
        if (dealer_card1 == 10|dealer_card1 ==11|dealer_card1 ==12|dealer_card1 ==13)
        {
           dealer_card1 = 10;
            
        }
        
        
	switch (dealer_card2) {
		case 1: cout << "Ace"; 
			break;
		case 11: cout << "Jack"; 
			break;
		case 12: cout << "Queen"; 
			break;
		case 13: cout << "King"; 
			break;
		default: cout << dealer_card2 << " and ";
			break;
}	
          if (dealer_card2 == 10|dealer_card2 ==11|dealer_card2 ==12|dealer_card2 ==13)
        {
           dealer_card2 = 10;
            
        }
        switch (dealer_card3){
		case 1: cout << " Ace"; 
			break;
		case 11: cout << " Jack" ; 
			break;
		case 12: cout << " Queen"; 
			break;
		case 13: cout << " King"; 
			break;
		default: cout << dealer_card3;
			break;
	
}

 if (dealer_card3 == 10|dealer_card3 ==11|dealer_card3 ==12|dealer_card3 ==13)
        {
            dealer_card3 = 10;
            
        }
      }
	cout << "\n" << "\n" << "Now you have ";
	switch (player_card1){
		case 1: cout << "Ace and "; 
		break;
		case 11: cout << "Jack and " ; 
			break;
		case 12: cout << "Queen and "; 
			break;
		case 13: cout << "King and "; 
			break;
		default: cout << player_card1 << " and "; 
			break;
}	
       if (player_card1 == 10|player_card1 ==11|player_card1 ==12|player_card1 ==13)
        {
            player_card1 = 10;
            
        }
 
            
        
	switch (player_card2){
		case 1: cout << "Ace"; 
			break;
		case 11: cout << "Jack" ; 
			break;
		case 12: cout << "Queen"; 
			break;
		case 13: cout << "King"; 
			break;
		default: cout << player_card2 << " and ";
			break;
	
}

 if (player_card2 == 10|player_card2 ==11|player_card2 ==12|player_card2 ==13)
        {
            player_card2 = 10;
            
        }
        	switch (player_card3){
		case 1: cout << " Ace"; 
			break;
		case 11: cout << " Jack" ; 
			break;
		case 12: cout << " Queen"; 
			break;
		case 13: cout << " King"; 
			break;
		default: cout << player_card3;
			break;
	
}

 if (player_card3 == 10|player_card3 ==11|player_card3 ==12|player_card3 ==13)
        {
            player_card3 = 10;
            
        }
       cout <<"\nDealer: " << dealer_total << " & " <<"Player: " << player_total;  //Shows the numbers in case if confusion
    }

        
          if (dealer_total > 21)
                        
                    cout<<"\n" << "The dealer is busted!";
                
          else if (player_total > 21)
              cout << "\n" << "You are busted!";
                
               
                
		else if (player_total > dealer_total)
				cout << "\n" << "You Win $" << wager << "!!" << endl;
                else if (player_total == dealer_total) 
                    cout<< "\n"<< "It is TIE!" << endl;
			else 
				cout << "\n" << "You lose $" << wager << "!" <<  endl;
              
}
                    }
 else
{
cout << "You did not enter a valid number!" << endl;
}
        }
    }
    }while (choice != CARDPOKER_CHOICE);
    return 0;
}

