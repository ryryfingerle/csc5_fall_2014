/* 
 * File:   main.cpp
 * Author: Owner
 *
 * Created on September 30, 2014, 11:39 AM
 */

#include <cstdlib>
#include <iostream>
#include <string>
#include <iomanip>
# include <algorithm>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    // Number 1 on LAB4P2
   
    
    string grade; // to hold the score 
    //Using the If/Else Statement since the switch statement (shown in the practice) do not work unless the integer
    
    //Get the grade
    cout << "Enter your grade and I will tell you the score: ";
            
    cin >> grade; 
    
    //Determine the score for the given grade
    if (grade == "A+" | grade == "a+")
    {
        cout << "Your score is 4.3 \n";
    }
    else if (grade == "A" | grade == "a")
 {
cout << "Your score is 4.0 \n";
 }
    else if (grade == "A-" | grade == "a-" )
 {
     cout << "Your score is 3.7 \n";
 }
    else if (grade == "B+" | grade == "b+" )
  {
      cout << "Your score is 3.3 \n";
  }
      else if (grade == "B" | grade == "b" )
  {
      cout << "Your score is 3.0 \n";
  }
    else if (grade == "B-" | grade == "b-" )
  {
      cout << "Your score is 2.7 \n";
  }
   else if (grade == "C+" | grade == "c+" )
  {
      cout << "Your score is 2.3 \n";
  }
      else if (grade == "C" | grade == "c" )
  {
      cout << "Your score is 2.0 \n";
  }
      else if (grade == "C-" | grade == "c-" )
  {
      cout << "Your score is 1.7 \n";
  }
      else if (grade == "D+" | grade == "d+" )
  {
      cout << "Your score is 1.4 \n";
  }
        else if (grade == "D" | grade == "d" )
  {
      cout << "Your score is 1.0 \n";
  }
        else if (grade == "D-" | grade == "d-" )
  {
      cout << "Your score is 0.7 \n";
  }
      else if (grade == "F" | grade == "f" )
  {
      cout << "Your score is 0.6 or less \n";
  }
      else
      {
          cout << "That grade is invalid. Who gave you a " << grade << "?\n";
      }

    // Number 2 on LAB4P2
    // Babylonian Algorithm
    
    int n, count(10);
double answer_n, guess, r, t;

// I am still working on this since I know BM equation as (r + x/r)/2
// Ex: for 27 similar to 25=5 so 27=5.2 
// r= 5.2 & x=27 then (5.2 + (27/5.2) /2) so the answer is 5.196
cout << "This program will compute the square root\n";
cout << "of a number using the Babylonian algorithm.\n";
cout << "Please enter a whole number:\n";
cin >> n;
cout << "Please enter a 'guess' number to divide by:\n";
cin >> guess;

t= n/2;
r = n/guess;
guess = (guess + r)/2;

while (count > 0)
{
r = n/guess;
guess = (guess + r)/2;

if (guess <= (guess * 0.01) + guess)
answer_n = guess;
else
r = n/guess;
guess = (guess + r)/2;

count-=1;
}


cout << "The square root of "<< n << " is " << answer_n;
cout << endl;

// Number 3 on Lab4P2 
// Leap Year program

int year;

       cout<<"\n Enter the year: ";
       cin>>year;

       if(year%4==0 || year%400==0 || year%100==0)
      cout<<"this is a leap year"<<endl;

       else
      cout<<"this is not a leap year"<<endl;

       //Number 5 on Lab4P2
       // Guessing Game Only 1-100
       
          int num;  // to hold the number
      num = rand() % 100 + 1;  //the specific number 
      int guess1; // user input
      do  // loop for the guessing 
      {
      cout << "Guess the number: ";  // prompt the user 
      cin >> guess1;
      if (guess1 < num)
      {
            cout << "It is too low, try again" << endl;
      }
      else if (guess1 > num)
      {
            cout << "It is too high, try again" << endl;
      }
      else
      {
            cout << "Your guess is right!" << endl;
      }
      } while (guess1 != num);
      
      
    return 0;
}

