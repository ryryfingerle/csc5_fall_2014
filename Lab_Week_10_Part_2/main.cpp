/* 
 * File:   main.cpp
 * Author: Owner
 * Name: Ryan Fingerle
 * ID: 2419802
 *
 * Created on October 30, 2014, 
 */

#include <cstdlib>
#include <iostream>
#include <string>
#include <fstream>


using namespace std;

// Number 3 on LBWK10P2   <<<<--------------
// Function that stores values 50-100 in array size 50

void store(int a[])
{
    for(int i = 0; i < 100 && i>50; i++)
    {
        a[i] = i; 
     }
}

void clearScreen()
{
    for(int i=0; i<5; i++)
    {
        cout << endl;
    }
}

// Number 4 on LBWK10P2  <<<<-----------------
// Function that check the user's value is inside of the array
void showValues(int [], int); //function prototype
/*
 * 
 */
int main(int argc, char** argv) {
    
     // Number 1 on LBWK10P2     <<<<-----------------
    // reads the file from the user and stores values into array
    string input; // contents in the file
     fstream userFile; // file stream object
     const int ARRAY_SIZE = 10; //Array SIZE 
     int numbers[ARRAY_SIZE]; // array with 10 elements
     
     
     int count = 0;  // loop counter variable
     
     
     
    
     //open the file in input mode
      userFile.open("array.read.txt", ios::in);
    
     // if the file opens successfully, continue
        if (userFile)
        {
            //read the numbers from the file into the array
         
            while(count <ARRAY_SIZE && userFile >> numbers[count])
                count++; 
          
            // close the file 
            userFile.close();
        }
        else 
        {
            cout << "ERROR: Cannot Read the File\n";
        }
// Display the numbers read: 
      cout << "The numbers are: "; 
      for(count = 0; count < ARRAY_SIZE; count++)
          cout << numbers[count] << " "; 
      cout << endl;
      
      clearScreen();   // to clear some space between each code for easier reading
              
              
              
      //I believe this is for number 4 on LABWK10P2  <<<<----------------------
      const int ARRAY_SIZES = 8; 
      int num[ARRAY_SIZES] = { 5, 10, 15, 20, 25, 30, 35, 40}; 
      
      cout << "Please enter any number that you think it might be in the array"<< endl;
      int input2; 
      cin >> input2; 
      
      cout << "Let's see if your number match to the numbers in the array" << endl;
      
       // Determine the value from the user is in the array or not
      if(input2 == num[ARRAY_SIZES])
      {
          cout << "Yes! Your number is in the array!" << endl;
      }
      else
      { 
          cout << "Sorry, your number is not in the array \n";
          showValues(num, ARRAY_SIZES); 
      }
      
     
      
    
      
    return 0;
}

void showValues(int nums[], int size)
{
    for(int index = 0; index < size; index++)
        cout << nums[index] << " "; 
    cout << endl;
}

