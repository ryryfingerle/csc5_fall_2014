/* 
 * File:   main.cpp
 * Author: Owner
 * Name: Ryan Fingerle
 * ID: 2419802
 * HW: HW5
 * Problem: Car Miles/ file I/O
 *
 * Created on October 7, 2014, 11:14 AM
 */

#include <cstdlib>
#include <iostream>
#include <string>
#include  <fstream>


using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    string response; 
    do
    { 
    double liters = 0.264179;
    double miles;
    int gallons; 
    double  avg;
    double avg2;
    
    ifstream infile;
    infile.open("data.dat.rtf");
    
    // Get the input from the file
    string input;
    // Use just like cin
    infile >> input;
    
    cout << "The info  is: " << input << endl;
    
    // Close
    infile.close();
    
    
    cout << "How many gallons do your car have? \n"; 
    cin >> gallons;
            
     avg = gallons * liters;
     
     cout << "Your car has " << avg << " liters in total.\n";
     
      cout << "How many miles do you drive? \n"; 
    cin >> miles;
     
       avg2 = miles * liters;
            
    cout << "You used up "<< avg2 << ".\n";
    
   
  
     cout <<"Would you like to go again? \n";
    
    cin >> response;
    }while(response == "yes");
    return 0;
}

