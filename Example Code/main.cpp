/* 
 * File:   main.cpp
 * Author: Owner
 * Name: Ryan Fingerle 
 * Code: Example for the classes
 *
 * Created on December 2, 2014, 10:56 AM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

class Vehicle
{
private:
    double engine; 
public: 
    Vehicle(double); 
    void output (ostream &);
};

class Car: Vehicle // : means inherits from
{
private: 
    string owner; 
public: 
    Car();
    Car(string, double); 
    void output(ostream &); 
};

Vehicle::Vehicle () // default constructor
{
    engine = 0; 
}
Vehicle::Vehicle (double engine)
: engine(engine) {}

void Vehicle::output (ostream& out)
{
    out << "Engine: " << engine << endl;
}

Car::Car ()
: Vehicle ()
{
    owner = " ";
}

Car::Car(String owner, double engine)
: Vehicle (engine), owner(owner) {}

void Car::ouput (ostream & out)
{
    Vehicle::ouput (out);
    out << "Owner: " << this ->owner << endl;
}

/*
 * 
 */
int main(int argc, char** argv) {
    
    Vehicle v;
    v.ouput(cout); cout << endl;
    
    Car c;
    c.ouput(cout); cout << endl;
    

    return 0;
}

