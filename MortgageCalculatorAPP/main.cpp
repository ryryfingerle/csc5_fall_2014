/* 
 * File:   main.cpp
 * Author: Owner
 * Name: Ryan Fingerle
 * ID: 2419802
 * 
 *I did not realize that it took a lot of work to create the mortgage calculator. 
 * Created on December 4, 2014, 2:46 AM
 */

#include <cstdlib>
#include <iostream>
#include <cmath>
#include <iomanip>
#include <limits>



using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    cout << " ==========>>> Ryan's Loan Calculator <<<============="<<endl;
    cout << " This app will help you figure whether or not if you could afford " << endl;
    cout  <<" a particular loan and see how much each month that you have to pay.\n" << endl;
    
    //Prompt the user for information 
    double homevalue;  // total amount of asking price for calculations
    double interestrate; 
    double loanamount; //total amount of loan for calculation 
    double downpayment; //user prompt for down payment 
    
    double annualincome; 
    double creditcardspayment;
    double carloans; 
    double carpayment;
    double studentloans; 
    double studentpayment; 
    double personalloans; 
    double personaloanpayment; 
    double t; // terms for calculation
double periods;
double payment;
double mRate;
double conversion;
const double months = 12;
int paymentNumber;
double bal;
int arraySelectTerm;
int arraySelectRate;
    // define 
double principle;
double term[] = {7, 15, 30};
double rate[] = {4.75, 5.5, 7.25};
double r;
double te;
int choice1;
int insurance;
int MonthlyIncome; // annual income calculated
int DebtRatio; 
float Number_of_Years; // for 7-20-30 years - user prompt
float MonthlyInterest; 
int number_Of_Payments; // for calculations  
float payment2; //Estimated amount to pay each month



    cout << "House Value  [The Asking/Listing Price]:  " << endl;
    cin >> homevalue; 
    cout << "Annual Income:    " << endl; 
    cin >> annualincome; 
    cout << "Interest Rate (%): " << endl;
    cin >> interestrate; 
    cout << "Loan Amount:  " << endl;
    cin >> loanamount;
    cout << "How many years [7-20-30]: " << endl;
    cin >> Number_of_Years;
    
    cout << "Down Payment (percent):  " << endl;
    cin >> downpayment; 
    
  //if you see the statements in grey area which means they are extra, possibly not needed. 
    cout << " Monthly Debt To Your Credit Cards: " << endl;
    cin >> creditcardspayment; 
    //cout << "Car Loans Amount Owed (Total Amount): " << endl;
    //cin >> carloans; 
    cout << "Monthly Payment To Your Car Loans: " << endl;
    cin >> carpayment; 
   // cout << "Student Loans Amount Owed (Total Amount): " << endl;
   // cin >> studentloans; 
    cout << "Monthly Payment To Your Student Loans: " << endl;
    cin >> studentpayment; 
    //cout << "Personal Loans Amount Owed (Total Amount): " << endl;
    //cin >> personalloans; 
    cout << "Monthly Payment To Your Personal Loans: " << endl;
    cin >> personaloanpayment; 
    
 //Calculate Grossed  Monthly Income
MonthlyIncome = (annualincome)/12;
double EstimatedDT;
double TotalDebt;
cout << "Your Monthly Income is $ " << MonthlyIncome << endl;
cout << endl;
TotalDebt = (creditcardspayment + carpayment + studentpayment + personaloanpayment);

cout << "Your Total Debt is " << TotalDebt << endl;

EstimatedDT = TotalDebt / MonthlyIncome;

cout << "Your Debt to Income is " << EstimatedDT << endl;
cout << endl;

//Calculate the total  debt to income 
//Still didn't calculate correctly 
DebtRatio = ((carpayment + studentpayment + personaloanpayment)/(MonthlyIncome));
if ( DebtRatio <= .43 ) 
{  
    cout << "Your Debt to Income is $ " << DebtRatio << endl; //decimal? 
    cout << "You qualify for the mortgage loan!" << endl;
}
else if  ( DebtRatio > .43)
{   
    cout << "Your Debt to Income is $ " << DebtRatio << endl; 
        cout << "I am sorry. You do not qualify for the mortgage loan" << endl;
}
        
        //Calculate the values
MonthlyInterest = (interestrate/100)/12; 

number_Of_Payments = Number_of_Years * 12; 



//Output 
cout << "For Loan Amount: $" << homevalue << " with interest rate: " << interestrate << " and a "
        << Number_of_Years << " year mortgage" << "." << endl;
cout << endl;


  


 cout << "Examples For You To Calculate the Rates in Excel" << endl;
 cout << endl;
cout << "Enter the amount of the mortgage in US dollars " << endl;
cout << endl;
cin >> principle;
cout << endl;

cout << "Select the loan terms " << endl;
cout << "0 - " << term[0] << " years and " << rate[0] << "%" << endl;
cout << "1 - " << term[1] << " years and " << rate[1] << "%" << endl;
cout << "2 - " << term[2] << " years and " << rate[2] << "%" << endl;
cin >> arraySelectTerm;
if (arraySelectTerm == 0) {
te = term[0];
r = rate[0];
}
if (arraySelectTerm == 1) {
te = term[1];
r = rate[1];
}
if (arraySelectTerm == 2) {
te = term[2];
r = rate[2];
}



// Output to be displayed
cout << "Mortgage amount: $" << principle << endl; //output amount
cout << "Mortgage term in years: " << te << endl; //output length of loan
cout << "Mortgage interest rate: " << r << endl; //output interest rate
cout << endl;
cout << endl;

// Convert to monthly interest rate
mRate = (r * .01) / months;

// Convert years to months
periods = te * months;

// Create variable for (1 + rate)periods
t = pow(1 + mRate, periods);

// Calculate payment
payment= (principle * t * mRate)/(t - 1);


    

// Output Monthly payment
cout << "Monthly payment is $" << payment << endl;

// Output Amortization
cout << "Loan Balance      Interest Paid     Principle Payment" << endl;
cout << "-----------------------------------------------------" << endl;
bal = principle; //initialize outside the loop
int count = 0;
for (paymentNumber = 1; paymentNumber <= periods; ++paymentNumber , count++)
{

 cout << bal << "          ";
 cout << bal *mRate << "          "; //interest payment
 cout << payment - bal*mRate << endl; //principle payment
 bal -= payment - bal*mRate; //subtract principle payment from bal
 if( count == 10)
 {
   cin.get();
   cout<<endl;
   cout << "Loan Balance      Interest Paid     Principle Payment" << endl;
   cout << "-----------------------------------------------------" << endl;

   count = 0;
 }


}



    return 0;
}

