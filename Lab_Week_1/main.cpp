/* 
 * File:   main.cpp
 * Author: Owner
 * Name: Ryan Fingerle
 * ID: 2419802
 *
 * Created on September 2, 2014
 */

#include <cstdlib>
#include <iostream>
#include <string>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    string name; 
    
    cout << "Hello, my name is Ryan!" << endl;
    cout << "What is your name?" << endl;
    cin >> name; 
    cout << "Hello " << name << ". I am glad to meet you." << endl;
    
    
    return 0;
}

