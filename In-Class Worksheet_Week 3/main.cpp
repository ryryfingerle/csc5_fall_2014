/* 
 * File:   main.cpp
 * Author: Owner
 *
 * Created on September 22, 2014, 5:47 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

double one = 19.1;

cout << setprecision(2) << one << endl;

cout << fixed << setprecision(3) << one << endl;

cout << setw(7) << one << endl;

cout << setfill('*') << setw(8) << one << endl;

}

