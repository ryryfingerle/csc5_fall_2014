/* 
 * File:   main.cpp
 * Author: Owner
 * Name: Ryan Fingerle
 * ID: 2419802
 *
 * Created on October 28, 2014, 11:54 AM
 */

#include <cstdlib>
#include <iostream>
#include <vector>


using namespace std;

//For Number 3 on LABWK8P2 <<<<------------
// Function that outputs any vectors
  void output(const vector<int> & v)
    {
        //For Loop iterates through all values in a vector
        for(int i=0; i<v.size(); i++)
        {
            cout << v[i] << endl;
        }
    }

  // Number 4 on the LABWK8P2  <<<<-----------
  //Function for Number 2 below
  void showValues(const vector<int> &v)
  {
      //for loop through the values in the input
      for(int i=0; i < v.size(); i++)
      {
          cout << v[i] << endl;
      }
  }// this function should work if not then I will fix it.. for now it is not called..
  
  
/*
 * 
 */
int main(int argc, char** argv) {
    
    //Number 1 on LABWK8P2
    //Write a program that creates a vector of ints and stores the values 3, 6, and 8 inside it
    
    vector<int> v;
    //add the elements
    
    v.push_back(3);
    v.push_back(6); 
    v.push_back(8);
    
    for(int i=0; i<v.size(); i++)
    {
        v[i] += 5; 
    }
    
    cout << "The values vector are: " << v.size() << endl;
    output(v);
    // in this code, the only output shows "3" not "6" and "8"
    
    // Number 2 on LABWK8P2        <<<<--------------------
    // Prompt the user for the number then store them in the vector
    //Continue till the user enter -1
     int Number; //Number by the user
    vector<int> data(Number);   //a vector of number for the user to input
    int index;   // loop counter
    
    //prompt the user for the data
    cout << "Please enter a number: " << Number << endl;
    cin >> Number; 
    for(index=0; index < 10; index++)
    {
        cout << endl;
    }
    //Display the number in the vector
    cout << "Here is the number that you input: "<< Number;
    //Repeat Code till the user input -1
    
    
    //Number 3 on LABWK8P2            <<<<-------------------
    // Write the function that output the contents of a vector of ints to the console
    //Shown above in the header
    
    //Number 4 on LABWK8P2       <<<<------------------------
    // Create a function that does number 2. Output the vector before and after the function call
    //shown above in the header 
  
    return 0;
}

