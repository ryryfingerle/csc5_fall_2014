/* 
 * File:   main.cpp
 * Author: Owner
 * Name: Ryan Fingerle
 * LABWK13
 *
 * Created on November 25, 2014,
 */

#include <cstdlib>
#include <iostream>
#include <iterator>
#include <algorithm>
#include <vector>
#include <iomanip>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    
 //Number 1 on LBWK13 
// Create two integer pointers in new data 
  int firstvalue = 45, secondvalue = 85;
  int * p1, * p2;

  p1 = &firstvalue;  // p1 = address of first value
  p2 = &secondvalue; // p2 = address of second value
  *p1 = 100;          // value pointed to by p1 = 100 
  *p2 = *p1;         // value pointed to by p2 = value pointed by p1
  p1 = p2;           // p1 = p2 (value of pointer is copied)
  *p1 = 200;          // value pointed by p1 = 200
  
  cout << "My first value is:  " << firstvalue << endl;
  cout << "My second value is:  " << secondvalue <<endl;
  
  //Number 2 on LBWK13 
  // Create a program that allows users input values into vector

 //int i;
  //vector<int> v(5);

  //cout << "Enter 5 integers: \n";
  
  //istream_iterator<int> int_itr(cin);
 // copy(int_itr, istream_iterator<int>(), v.begin());

  //cout << "Here are the values you entered: ";
  //for(i = 0; i <v.size(); i++) cout << v[ i ] << " ";
  
  
  // Number 3 on LBWK13
  //
  
  
  
  // Number 4 on LBWK13
  // Create two integer pointers in new data
  
   int firstnum = 100, secondnum = 200;
  int * n1, * n2;

  n1 = &firstnum;  // n1 = address of first number
  n2 = &secondnum; // n2 = address of second number
  *n1 = 75;          // number pointed to by n1 = 75
  *n2 = *n1;         // number pointed to by n2 = number pointed by n1
  n1 = n2;           // n1 = n2 (number of pointer is copied)
  *n1 = 185;          // number pointed by n1 = 185
  
  cout << "My first value is:  " << firstnum << endl;
  cout << "My second value is:  " << secondnum <<endl;
  
  // Number 5 on LBWK13 
  //Create a static array and pointer. Have the pointer point the same location as array.
  // Output both the array and pointer 
  
  const int NUM = 5; 
  double numbers[NUM] = { 5, 10, 15, 20, 25};
  double *doublePtr;    // pointer to the number 
  int count;                       // Array Index
  
  // Assign the address of the numbers array to doublePtr
  doublePtr = numbers; 
  
  //Display the contents of the numbers array. 
  cout << "Here are the values in the numbers array: \n"; 
  for(count = 0; count < NUM; count++)
      cout << doublePtr[count] << " "; 
  
  //Display the contents of the array again
  cout << " \nAnd here they are again: \n"; 
  for (count =0; count < NUM; count++ )
      cout << *(numbers + count) << " "; 
  cout << endl;
  
  //Number 6 on LBWK13
  // Create dynamic array with a pointer 
  
  return 0;
}

