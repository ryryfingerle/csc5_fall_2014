/* 
 * File:   main.cpp
 * Author: Owner
 * Name: Ryan Fingerle
 *Game: TTT 
 * Created on December 3, 2014
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>


using namespace std;
char square[10] = {'0','1','2','3','4','5','6','7','8','9'};
void clearscreen()  //Clear the screen each menu and game 
{
    for (int i =0; i<25; i++)
    {
        cout << endl;
    }
}
int checkwinorlose();
void board();
/*
 * 
 */
int main(int argc, char** argv) {
    
     // Constants for Menu Choices
    const int TICTACTOE_OPTION = 1, 
                       MORTGAGE_OPTION = 2;
            
 // Variables
    int OPTION;   // Menu Choice
    
    // Set up Numeric Output Formatting 
    cout << fixed << showpoint << setprecision(2);
    
    //Display the Menu 
    do
    {
        clearscreen();
    cout << "\n\t\tLet's Play a Game or Calculate Your Estimate Mortgage Cost! \n\n"
            << "1. TIC TAC TOE  (If invalid then press 1 again) \n" //kept getting output invalid choice even though 1 is not less than
            << "2. MORTGAGE CALCULATOR\n"
            
          
            << "Enter your choice: "; 
    cin >> OPTION;
    while (OPTION < TICTACTOE_OPTION || OPTION > MORTGAGE_OPTION);
        
    
        cout << "Please enter a valid menu choice: "; 
        cin >> OPTION;
    
    // Process the User's choice
    if (OPTION == TICTACTOE_OPTION)
    {
   clearscreen(); 
  //Variables for tic tac toe
 int player = 1;
 int i;
 int choice;
 
char mark;
do
{
board();
player=(player%2)?1:2;



cout << "Player " << player << ", enter a number:  ";
cin >> choice;
clearscreen();
mark=(player == 1) ? 'X' : 'O';

if (choice == 1 && square[1] == '1')

square[1] = mark;
else if (choice == 2 && square[2] == '2')

square[2] = mark;
else if (choice == 3 && square[3] == '3')

square[3] = mark;
else if (choice == 4 && square[4] == '4')

square[4] = mark;
else if (choice == 5 && square[5] == '5')

square[5] = mark;
else if (choice == 6 && square[6] == '6')

square[6] = mark;
else if (choice == 7 && square[7] == '7')

square[7] = mark;
else if (choice == 8 && square[8] == '8')

square[8] = mark;
else if (choice == 9 && square[9] == '9')

square[9] = mark;
else
{
cout<<"Invalid move ";

player--;

}
i=checkwinorlose();

player++;
}while(i==-1);
board();
if(i==1)

cout<<"==>\a     Player "<<--player<<" wins!!   <== ";
else
cout<<"==>\a     Game Draw    <==";
    }
     if (OPTION == MORTGAGE_OPTION)
    {
        clearscreen();
     }    

 }while (OPTION != TICTACTOE_OPTION);
 
 
return 0;
}

/*********************************************
 * Functions
**********************************************/

int checkwinorlose()
{
	if (square[1] == square[2] && square[2] == square[3])

		return 1;
	else if (square[4] == square[5] && square[5] == square[6])

		return 1;
	else if (square[7] == square[8] && square[8] == square[9])

		return 1;
	else if (square[1] == square[4] && square[4] == square[7])

		return 1;
	else if (square[2] == square[5] && square[5] == square[8])

		return 1;
	else if (square[3] == square[6] && square[6] == square[9])

		return 1;
	else if (square[1] == square[5] && square[5] == square[9])

		return 1;
	else if (square[3] == square[5] && square[5] == square[7])

		return 1;
	else if (square[1] != '1' && square[2] != '2' && square[3] != '3' 
                    && square[4] != '4' && square[5] != '5' && square[6] != '6' 
                  && square[7] != '7' && square[8] != '8' && square[9] != '9')

		return 0;
	else
		return -1;
}


/******************************************
     FUNCTION TO DRAW BOARD OF TIC TAC TOE 
*******************************************/


void board()
{

cout << "\n\n\tRyan's Tic Tac Toe\n\n";

cout << "Player 1 (X)  -  Player 2 (O)" << endl << endl;
cout << endl;

cout << "     |     |     " << endl;
cout << "  " << square[1] << "  |  " << square[2] << "  |  " << square[3] << endl;

cout << "_____|_____|_____" << endl;
cout << "     |     |     " << endl;

cout << "  " << square[4] << "  |  " << square[5] << "  |  " << square[6] << endl;

cout << "_____|_____|_____" << endl;
cout << "     |     |     " << endl;

cout << "  " << square[7] << "  |  " << square[8] << "  |  " << square[9] << endl;

cout << "     |     |     " << endl << endl;

    
}

