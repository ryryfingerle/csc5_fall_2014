/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 4, 2014, 11:10 AM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

int main(int argc, char** argv) {
    
    // Number 1 on Lab Week 2 
    cout << "Hello, my name is Hal" << endl;
    
    cout << "What is your name?" << endl;
    
    string name;
    
    cin >> name;
    
    cout << "Hello, " << name << ". I am glad to meet you." << endl;
    
    // Number Two on Lab Week 2 
    
    int a=5;
    int b=10;
    
    cout << "a: " << a << "" << "b: " << b << endl;
    
    int temp = a;
    a=b;
    b=temp;
    
    cout << "a: " << a << "" << "b: " << b << endl;
    
    // Miles and Meters - Number 3 on Lab Week 2
    
    int  mile=1609.344;
   int  meter=3.281; 
    
    cout << "One mile equals " << mile << " meters." << endl;
    cout << "One meter equals " << meter << " feet." << endl;
    
    // Number Four on Lab Week 2
    
    int x = 5;
    x = x + 5; 
    cout << x << endl;
    
    // Trying the Test Scores
    
    cout << "Your average scores for your tests" << endl;
    
    cout << "What is your first test score?" << endl;
    
    int number1;
    int number2;
    int number3;
    
    cin >> number1;
    
    cout << "What is your second test score?" << endl;
    
    cin >> number2;
    
    cout << "What is your third test score?" << endl;
    
    cin >> number3;
    
    double avg = (number1 + number2 + number3) / 3;
   
    
    cout << "Your average is " << avg << endl;
    
    // Number 5 Part B on Lab Week 2
    
    int score1, score2, score3; // the test scores 
     avg; // to hold the average score
    
    // Get the three test scores at one time. 
    cout << "Enter 3 test scores and I will give average overall score: ";
    
    cin >> score1 >> score2 >> score3;
    
    // Calculate and Display the average score.
    double average = (score1 + score2 + score3)/ 3;
    
    cout << "Your average is " << average << endl;
    
    // Number 6 on Lab Week 2 
    // Program that repeats the user input except the ouput should be spaced
    
    cout << "Enter the numbers that you wish to put" << endl; 
    cout << "Any positive numbers so I can show you the different output" << endl;
    int number4;
    int number5; 
    int number6; 
    int number7; 
    int number8; 
    
    cin >> number4 >>number5 >> number6 >> number7 >> number8; 
    
    cout << number4;
    cout << "  ";
    cout <<  number5; 
    cout << "  ";
            cout << number6; 
    cout << "  ";
            cout << number7; 
    cout << "  ";
    cout << number8; 
    
    // Looks like each number must be entered by the space or 'enter' in order to get more than 1 number
    

    return 0;
}

