/* 
 * File:   main.cpp
 * Author: Owner
 * Name: Ryan Fingerle 
 * ID: 2419802
 *  HW: HW6 
 * Problem: Function
 * 
 * Created on October 9, 2014, 12:28 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

// Function Prototypes
int square (int);
double square (double); 
 
int main(int argc, char** argv) {
    
    // Hold the figures
    int userInt;  // numbers
    double userFloat;  // for the decimals   (should I do both figures as well?)
    
    cout << fixed << showpoint << setprecision(2); // formatting in case if user inputs the decimals 
    cout << "Enter two numbers: ";  // Prompt the user for the numbers
    cin >> userInt >> userFloat;  // input the numbers
    
    cout << "Here are their squares: ";   // showing the output of their numbers
    cout << square(userInt) << " and " << square(userFloat); 
    
    return 0;
}

// Testing the definition of overloading according to the book 

int square (int number) // for the numbers 

{
    return number * number; 
}

double square (double number)    // for the decimals 
{
    return number * number; 
} 



