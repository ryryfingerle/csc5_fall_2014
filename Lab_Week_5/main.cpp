/* 
 * File:   main.cpp
 * Author: Owner
 *
 * Created on September 22, 2014
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    //Number 1 on Lab5
    //interest
    
    double monthlyPayment;
	double totalAmount;  // total amount or balance that the user owes
	double interestRate;   // the user's interest rate
	double interestPaid;  // total amount of interest after the user paid
	double initialBalance; // 
	double termOfLoan;
	int month = 1;   //Suppose to be the  number of months
        char ans;   // for the loop so the user can repeat

	cout << fixed << showpoint << setprecision(2);
        do
        {
	cout << "Enter the amount you owe: $ ";
	cin >> totalAmount;
	cout << "Enter the interest rate : ";
	cin >> interestRate;
	cout << "Enter your monthly payment : $ ";
	cin >> monthlyPayment;
	
	initialBalance = totalAmount;

	while (interestRate >= 1)       //Converts the interest rate to a decimal if the user inputs 
                                                                                    //in percentage form
	{ 
		interestRate = interestRate / 100; 
	}

	totalAmount = totalAmount * (1 + interestRate / 12) - monthlyPayment;
		
	cout << "After month 1 your balance is $" << totalAmount << endl;

	while (totalAmount > 0)
	{
		if (totalAmount < monthlyPayment)
		{
			totalAmount = totalAmount - totalAmount;
		}	
		else 
		{
			totalAmount = totalAmount * (1 + interestRate / 12) - monthlyPayment;
		}

		month = month++;	// monthly increment 	

		cout << "After month " << month << ", your balance is : $" << totalAmount << endl;
		}
		cout << "You have paid off the loan at this point. Congratulations!" << endl;
		
		
		termOfLoan = month;
		
interestPaid = (monthlyPayment * termOfLoan) - initialBalance; 		

/*I couldn't figure out how to get the months to be numbered instead of showing 
 * 1 each month. I will get back to this*/
 
        	cout << "You paid a total amount of $" << interestPaid << " in interest." << endl;
	        // Total Interest paid by the user
                cout << "Would you like to calculate again? Y/N \n"; //prompt the user to go again
                cin >> ans;
        }while (ans =='Y' || ans == 'y');
        
        
        // Number 2 on Lab5
        // 23 Toothpick Game 
        
         int c_pick;   // computer 
         int h_pick;  // human 
         int toothpicks= 23;  // number of toothpicks 

cout << "Welcome to the toothpick game." << endl;
do{ 
    
   
        cout << "Please pick up your toothpick(s), choose between 1 and 3.\n";
        cin >> h_pick;

        toothpicks = toothpicks - h_pick;   // reduce the number of the total from human picks
        cout << toothpicks << " toothpick(s) remaining" << endl;

        if (toothpicks == 1 && c_pick == 1) {
            cout << "Human! You have prevailed!" << endl;
            break;
        } 

        if (toothpicks > 4) {
            c_pick = 4 - h_pick;
            cout << "The computer took " << c_pick << " toothpick(s)" << endl;
        } else if (toothpicks == 2) {
            c_pick = 1;
            cout << "The computer took " << c_pick << " toothpick(s)" << endl;
        } else if (toothpicks == 3) {
            c_pick = 2;
            cout << "The computer took " << c_pick << " toothpick(s)" << endl;
        } else if (toothpicks == 4) {
            c_pick = 3;
            cout << "The computer took " << c_pick << " toothpick(s)" << endl;
        } else if (toothpicks == 1) {
            c_pick = 1;
            cout << "The computer took " << c_pick << " toothpick(s)" << endl;
        }

        toothpicks = toothpicks - c_pick;  // reduce the number of the total from computer picks
        cout << toothpicks << " toothpick(s) remaining"<<endl;

        if (toothpicks == 1 && h_pick == 1) {
            cout << "The computer has prevailed!" << endl;
            break;
        }
        
        if (h_pick>3 || h_pick <0)
        {
            cout<< "Please enter a correct value." <<endl;
            
            continue;
        }
        
        if (toothpicks <0){
            cout<<"Game will restart, now. (Enter 4 to quit)"<< endl;
            cin>>h_pick;
            toothpicks=23;
            continue;
        }

    }
    while (h_pick != 4);
    
    // Number 3 on Lab5
    // Palindrome
    
   string words;

	cout <<"Enter words : " ;
	getline( cin,words );
        cin >> words;

	string wordsReverse( words.rbegin() , words.rend() );
	cout << "\nBackwards , it's : " << wordsReverse << endl << endl;


	if( words == wordsReverse )
		cout << words << " is a palindrome !" << endl;
	else
		cout << words << " is not a palindrome ! " << endl;
        
    //Number 4 on Lab5
    // Nested Loops Stars
    //Unfortunately I couldn't figure out where to put the dash in this code
    
    //outer loop 
    for (int i=0; i<6; i++)
    {
        //inner loop
        for (int j=0; j<13; j++)
        {
            cout << "*";
        }
        cout << endl;
    }
    
    return 0;
}

