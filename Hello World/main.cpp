/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on August 28, 2014, 12:10 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

int main(int argc, char** argv) {

    cout << "What is your name?" <<endl;
    
    string name;
    
    cin >> name;
    
    cout << "Hello, " << name << ". Nice to meet you";
    
    return 0;
}

