/* 
 * File:   main.cpp
 * Author: Owner
 * Name: Ryan Fingerle
 * ID: 2419802
 * HW: HW7 
 * Problem: 
 *
 * Created on October 28, 2014
 */

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>


using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    
    //Number 1 on HW7 
    //Write the code that searches a file of numbers of type int
    // Write the largest numbers and the smallest numbers
    
    string input; // contents in the file
     fstream nameFile; // file stream object 
    
     //open the file in input mode
      nameFile.open("Data.dat.txt", ios::in);
    
     // if the file opens successfully, continue
        if (nameFile)
        {
            //read the line in the file 
            getline(nameFile, input);
            
            //read successfully
            while(nameFile)
            {
                //Display the last item read
                cout << input << endl; 
                
                //read the next item
                getline(nameFile, input);
            }
            // close the file 
            nameFile.close();
        }
        else 
        {
            cout << "ERROR: Cannot Read the File\n";
        }
      
      //Number 2 on HW7 
      // Get the file from the user 
      
      
      //Number 3 on HW7 
      

    return 0;
}

