/* 
 * File:   main.cpp
 * Author: Owner
 * Ryan Fingerle
 * ID: 2419802
 * 
 * Problem: 
 *
 * Created on November 20, 2014, 11:34 AM
 */

#include <cstdlib>

using namespace std;

/* create and return a 2d dynamic array
 */

int** create2DArray(int row, int col)
{
    //create the dynamic array of pointers
    int**p = new int *[row];
            
            // create the internal data
    for(int i = 0; i  <row; i++)
    {
        p[i] = new int[col]; 
    }
    
    return p;
}
//fills a 2D Dynamic array

void fill(int**p, int row, int col)
{
    for (int i = 0; i<row; i++)
    {
        for(int j =0; j < col; j++)
        {
            p[i][j] = i * col + j +1;
        }
    }
}

//outputs a Dynamic Array
void output(int**p, int row, int col)
{
    for (int i = 0; i<row; i++)
    {
        for(int j =0; j < col; j++)
        {
           cout <<  p[i][j] " ";
        } 
        cout << endl;
    }
    
}

/*
 * 
 */
int main(int argc, char** argv) {
    
    
     int row = 6; 
    int col = 10; 
    
    int **p = create2DArray(row, col);
    fill (p, row, col);
    output (p, row, col);
    

    return 0;
}

