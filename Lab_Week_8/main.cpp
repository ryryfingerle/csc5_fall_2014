/* 
 * File:   main.cpp
 * Author: Owner
 * Name: Ryan Fingerle
 * LAB WEEK 8 
 * 2419802
 *
 * Created on October 21, 2014, 11:40 AM
 */

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>

using namespace std;

void clearScreen()
{
    for(int i=0; i<5;  i++)
    {
        cout << endl;
    }
}

/*
 * 
 */
int main(int argc, char** argv) {
    
    string input; // to hold the file input 
    fstream nameFile; // file stream object 
    char ch; // to hold the character 
    
    
    //Number 1 on LAB WEEK 8 (READ the characters from the file)
    //open the file in INPUT MODE
    nameFile.open("output.dat.txt", ios::in);
    
    //If the file opens successfully, continue
    if(nameFile)
    {
        //Read the file contents 
        while(nameFile >> input)
        {
            cout << input; 
        }
        
        //Close the file
        nameFile.close();
    }
    else
    {
        cout << "ERROR: Cannot Open File\n";
    }
    
    
    
    clearScreen();
    
    
    
    // Perfect for Number 2 on LAB Week 8 (Prompt the user to write onto the file)
    // to open the file in the input mode
    fstream dataFile("output.dat.txt", ios::out);
    
    cout << "\nPlease type the sentences that you wish to be read and saved to the file \n";
    cout << "You can type as many as you want as long as you end it with @ \n";
    
    //get the input from the user and write each character to the file
    cin.get(ch);
    while (ch != '@')
    {
        dataFile.put(ch);
        cin.get(ch);
    }
    
    dataFile.put(ch); // Write the symbol @
    
   
        //Close the file 
        dataFile.close();
        
        clearScreen();
        
        //Number 3 on LAB WEEK 8 (Define the difference between cin.get()/cin>>/getline(cin, string))
        
        cout << " cin.get() is also called get member function that reads single character of input including white spaces\n";
        cout << " cin >> reads a single character of input, ignoring the white spaces\n"; 
        cout << " getline(cin,string) reads an entire line including leading and embedded spaces & stores in an string object\n";
        
        clearScreen();
        
    // Number 4 on LAB WEEK 8 (Reads from a file as input by the user and prints the contents on the screen)
        
        //Open the file in the input mode 
        nameFile.open("user.input.txt", ios::in);
        
        // if the file opens successfully, continue
        if (nameFile)
        {
            //read the line in the file 
            getline(nameFile, input);
            
            //read successfully
            while(nameFile)
            {
                //Display the last item read
                cout << input << endl; 
                
                //read the next item
                getline(nameFile, input);
            }
            // close the file 
            nameFile.close();
        }
        else 
        {
            cout << "ERROR: Cannot Read the File\n";
        }

        clearScreen(); 
        
        //Number 5 on LAB WEEK 8 (changing the uppercases to lowercases)
        
         std:: ifstream theFile;
         theFile.open ("char.input.txt",  ios::in);
        std::  string theLine; 
         char j;
         
        
    
    //If the file opens successfully, continue
    if(theFile)
    {
        //Read the file contents 
      /*  while(theFile >> input)
        {
            cout << input; 
        }
        */
        while (theFile >> theLine)
         {
           //  theFile >> theLine; 
             for (size_t j=0; j < theLine.length(); ++j)
             {
                 theLine[j] = tolower(theLine[j]);
             }
             std::cout<<theLine<<std::endl;
         }
        
        //Close the file
        theFile.close();
    }
    else
    {
        cout << "ERROR: Cannot Open File\n";
    }
       
         
         
         
         
         
         
         
         
         
         /*
         while (!theFile.get(j))
         {
             theFile >> theLine; 
             for (size_t j=0; j < theLine.length(); ++j)
             {
                 theLine[j] = tolower(theLine[j]);
             }
         }
      */
    
    return 0;
}

