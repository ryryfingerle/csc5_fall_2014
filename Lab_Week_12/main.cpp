/* 
 * File:   main.cpp
 * Author: Owner
 * Ryan Fingerle
 * ID: 2419802
 * 
 * Problem: number 4
 *
 * Created on November 13, 2014
 */

#include <cstdlib>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

//Number 1 on LBWK12 
// Function for the Bubble Sort

void sortArray(int [], int); 
void showArray(const int [], int); 

//Number 2 on LBWK12
//Function for the Selection Sort

void selectionSort(int [], int); 
void showSelection(const int [], int);



/* 
 * 
 */
int main(int argc, char** argv) {
    
   // Number 1 on LabWK 12 
    //Implement the bubble sort algorithm from last week’s lecture for an array
    
   //Array of unsorted values
    int values [6] = {7, 2, 3, 8, 9, 1}; 
    
    //Display the values 
    cout << "The unsorted values for Bubble Sort are: \n"; 
    showArray(values, 6); 
    
    //sort the values
    sortArray(values, 6);
    
    //Display them again 
    cout << "The sorted values Bubble Sort are: \n"; 
    showArray(values, 6);
    
    
    // Number 2 on LabWK 12 
    //Implement the selection sort algorithm from last week’s lecture for an array

    //Define an array with unsorted values
    const int SIZE = 6; 
    int numbers [SIZE] = {5, 7, 2, 8, 9, 1}; 
    
    //Display the values
    cout << "The unsorted values for Selection Sort are: \n"; 
    showSelection(numbers, SIZE); 
    
    //Sort the values
    selectionSort(numbers, SIZE); 
    
    //Display the values again
    cout << "The sorted values for Selection Sort are: \n"; 
    showSelection(numbers, SIZE);
    
    //Number 3 on LabWK 12 
    // Create a multi-dimensional array representing the number of scores from a file. Provide the file with the following data: The number of students, the number of tests, and the number of corresponding scores
    
    const int NUM_STUDENTS =3; 
    const int NUM_SCORES = 5; 
    double total; 
    double average; 
    double scores [NUM_STUDENTS][NUM_SCORES] = {{88, 97, 79, 86, 94}, 
    { 86, 91, 78, 79, 84}, 
    { 82, 73, 77, 82, 89}}; 
    
    //Get the class average 
    for(int col =0; col< NUM_SCORES; col++)
    {
        //reset the accumulator
        total = 0; 
        //sum the column
        for(int row =0;  row <NUM_STUDENTS; row++)
            total += scores[row][col];
        //get the average
        average = total/NUM_STUDENTS;
        //Display the class average 
        cout << "Class average for test " << (col +1)
                 << " is " << average << endl;
        
    }
    
    // Number 4 on LabWK 12 
    //Implement the binary search algorithm in an ordered vector of size 100000
    
 /*   int binarySearch(int val; const vector<int>& v)
    {
        int start = 0; 
        int stop = v.size() -1; 
        int loc = -1; 
        
        while(start<stop && loc 1 = -1)
        {
            int guessloc = (start + stop)/2;
            int guess = v[guessloc]; 
            if(guess == val) // if found
                loc = guessloc
          }
        else
            {
                //First-Half
                if(guess>val)
                {
                    stop=guess loc-1;
                }
                else // second half
                {
                    start = guessloc +1; 
                }
            }
            
        
    }*/
    
    //Number 5 on LBWK 12 
    //Create two integer pointers that point to new data. Output the pointers value and their respective addresses
    
    const int SIZ = 8; 
    int set [SIZ] = {5, 10, 15, 20, 25, 30, 35, 40}; 
    int *numPtr;
    int count; 
    
    numPtr = set; 
    
    //Use pointer to display the array
    cout << "The numbers in the set are: \n"; 
    for (count =0; count < SIZ; count++)
    { 
        cout << *numPtr << " "; 
        numPtr++; 
        
    }
    
    cout << "The numbers in the set backwards are: \n"; 
    for (count =0; count <SIZ; count ++)
    { 
        numPtr--; 
        cout<< *numPtr << " ";
    }
    
    //Number 6 on LBWK 12 
    //ask for the user to insert numbers in the vector (504)
    
    return 0;
}

void sortArray(int array[], int size)
{
    bool swap;
    int temp;
    
    do 
    { 
        swap = false; 
        for(int count = 0; count < (size -1); count++)
        {
            if(array[count] > array[count +1])
            {
                temp = array[count]; 
                array[count] = array[count +1]; 
                array[count +1] = temp; 
                swap = true; 
            }
        }
    }while (swap); 

}

void showArray(const int array[], int size)
{ 
    for(int count = 0; count<size; count++)
        cout << array[count] << " "; 
    cout<< endl;
}

void selectionSort(int array[], int size)
{
    int startScan, minIndex, minValue;
    for(startScan = 0; startScan <(size - 1); startScan++)
    { 
        minIndex = startScan; 
        minValue = array[startScan]; 
        for(int index = startScan + 1; index <size; index++)
        { 
            if(array[index] <minValue)
            {
                minValue = array[index];
                        minIndex = index;
            }
        }
        
        array[minIndex] = array[startScan]; 
        array[startScan] = minValue;
    }
}

void showSelection(const int array[], int size)
{
    for(int count = 0; count < size; count++)
        cout << array[count] << " ";
    cout << endl;
}