/* 
 * File:   main.cpp
 * Author: Owner
 *Name: Ryan Fingerle 
 *Student ID: 2419802
 *Date: 9/24/14
 *HW: HW3
 *Problem: Creating a phone number -  fruit numbers - room capacity - numbers greater than zeros
 * I certify this is my own work and code
 * Created on September 24, 2014, 4:39 PM
 */

#include <cstdlib>
#include <iostream> 
#include <string> 
#include <exception>
#include <algorithm>


using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
// Phone Number 

string phonenumber;

cout << "Enter your 7 digit phone number with a dash\n"; 
cin >> phonenumber; 

int loc = phonenumber.find("-");
string firstHalf = phonenumber.substr(0,3);

string exchange = phonenumber; 

cout << "Formatted Number is: " << exchange << endl;


// Number 2 on HW3
// 

int apples;   // to hold the number from the user's input
int pears;    // ^^
int oranges; //^^
int apples2;     // to get the correct number in the else statement
int pears2;     //^^
int oranges2;    //^^

// prompt the user to input the numbers of each 
cout << "Enter the number of apples: " << endl;
cin >> apples; 
cout << "Enter the number of pears: " << endl; 
cin >> pears; 
cout << "Enter the number of oranges: " << endl;
cin >> oranges;

if (apples < 20)
{                                                                           
cout << "Enjoy your apples! \n";
}
else
{                                                                         
apples2 = (20 - apples) * -1;
cout << "Please leave the remaining of " << apples2 << " apples.\n"; 
} 
if (pears < 10)
{
cout << "Yum! These pears are delicious!\n";
}
else 
{ 
pears2 = (10 - pears)* -1;
cout << "Please leave some pears: " << pears2 << endl;
} 
if (oranges < 15)
{
cout << "Oranges are excellent for your health!\n"; 
} 
else 
{ 
oranges2 = (15 - oranges)* -1;
cout << "Please discard the remaining of " << oranges2 << " oranges.\n"; 
} 
 
// Number 3 on HW3 
// Write the program to show the fire law regulations
// Violations if there are more than the required number of people in a room 

int people; 
int people2; 

cout << "How many people are in the room?\n";
cin >> people; 

if (people < 150) 
{ 
cout << "You are ok for now...";
}
else 
{ 
cout << "You are in a violation of the law!\n"; 
people2 = (150 - people)* -1;
cout << "Please ask any " << people2 << " people to leave the room at this moment." << endl;
}

// Number 4 on HW3
    // Prompt the user to input any number 10 times
//my 1st attempt


cout << "Enter a number: "; 
int nums; 
cin >> nums; 

cout << nums << endl;

cout << "Enter a number: "; 
int nums2; 
cin >> nums2; 

cout << nums2 << endl;

cout << "Enter a number: "; 
int nums3; 
cin >> nums3; 

cout << nums3 << endl;    

cout << "Enter a number: "; 
int nums4;
cin >> nums4;

cout << nums4<< endl;

cout << "Enter a number: "; 
int nums5;
cin >> nums5;

cout << nums5<< endl;

cout << "Enter a number: "; 
int nums6;
cin >> nums6;

cout << nums6<< endl;

cout << "Enter a number: "; 
int nums7;
cin >> nums7;

cout << nums7<< endl;

cout << "Enter a number: "; 
int nums8;
cin >> nums8;

cout << nums8<< endl;

cout << "Enter a number: "; 
int nums9;
cin >> nums9;

cout << nums9<< endl;

cout << "Enter a number: "; 
int nums10;
cin >> nums10;

cout << nums10 <<endl;

// Problem 4 for HW3
// Example provided by Mung
// Continue on adding the code to complete this

cout << "Enter a grade"; 
string grade;
cin >> grade; 
string letter = grade.substr(0,1);  => grade [0], 
string mod = grade.substr(1);
if (letter == "A") 
{
if(mod=="+")
cout << 4.0; 
else if(mod=="-")
cout << 3.7; 
else 
cout << 4.0;
} 

// Subscript operator
string name = "Hello"; 
cout << name << endl; 
name [1] = 'B'; 
cout << name << endl; 

// HW3 #4 2nd attempt 
//For loop to run 1o times

int posSum = 0; 
int negSum = 0; 

for(int i = 0; i <10; i++)
{
cout << "Enter a number: "; 
int num; 
cin >> num; 

cout << "You entered: " << num << endl;



// to determine of number is positive 
if (num >0) 
{
posSum +=num; 
} 
else 
{ 
//negSum +=num; 
negSum = negSum + num;
} 
{
totalSum += num; 
}
}

cout << "Positive Sum: " << posSum<< endl;
cout << "Negative Sum: " << negSum << endl;
cout << "Total Sum: " << totalSum << endl;

return 0;
}

